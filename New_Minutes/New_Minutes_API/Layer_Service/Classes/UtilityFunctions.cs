﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes_API.Layer_Service.Classes
{
    public class UtilityFunctions
    {
        public string[] holidays = new string[] { "Sunday", "Saturday"};
        public void CopyAll<T>(T source, T target)
        {
            var type = typeof(T);
            foreach (var sourceProperty in type.GetProperties())
            {
                var targetProperty = type.GetProperty(sourceProperty.Name);
                targetProperty.SetValue(target, sourceProperty.GetValue(source, null), null);
            }
            foreach (var sourceField in type.GetFields())
            {
                var targetField = type.GetField(sourceField.Name);
                targetField.SetValue(target, sourceField.GetValue(source));
            }
        }
       // https://stackoverflow.com/questions/1617049/calculate-the-number-of-business-days-between-two-dates
        public int getNumberOfWorkingDays(DateTime from,DateTime to)
        {

            var dayDifference = (int)to.Subtract(from).TotalDays;
            int s= Enumerable
                .Range(1, dayDifference)
                .Select(x => from.AddDays(x))
                .Count(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday);
            return s + 1;
            
        }

    }
}