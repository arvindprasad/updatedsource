﻿using New_Minutes_API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using New_Minutes_API.Layer_Service.Classes;
using New_Minutes_API.Layer_Service;
using System.Web.Http.Cors;
using New_Minutes_API.Models.Custom_models;
using System.IO;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using static New_Minutes_API.Layer_Service.Classes.ProjectData;
using New_Minutes_API.Models.Join_Custom_models;
using New_Minutes_API.ActionFilters;

namespace New_Minutes_API.Controllers
{
    public class ProjectResourceAllocationController : ApiController
    {

        public IEnumerable<object> InsertProjectBudgetedResource(List<project_resource_allocation> prjBudRes)
        {
         
            int success = 0;
            using (var context = new minutesEntities(false))
            {
                foreach(project_resource_allocation pra in prjBudRes)
                {
                    context.project_resource_allocation.Add(pra);
                }
                success = context.SaveChanges();
            }
            // return null;
            List<object> list = new List<object>() { (success > 0) ? ResultProcesser.ProcessResult(true) : ResultProcesser.ProcessResult(false) };
            if (success > 0)
            {
                Dictionary<string, int> result = new Dictionary<string, int>();

                list.Add(result);
            }
            return list;
        }





    }
}