﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes_API.Models
{
    public class ResultProcesser
    {
        public static dynamic ProcessResult(bool success)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            if (success)
            {
                result.Add("result", 1);
            }
            else {
                result.Add("result", 0);
            }
            return result;
        }
        public static List<object>  isNotDeletableRecord()
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            result.Add("result", -1);
            List<object> list1 = new List<object>() { result };
            return list1;
        }
    }
}