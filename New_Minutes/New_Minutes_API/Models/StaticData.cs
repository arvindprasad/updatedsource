﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes_API.Models
{
    public class StaticData
    {
        public const string actionItemCompletedID = "AI_COMPLETED";

        public const int actionItemProgressOnTimeID = 1;
        public const int actionItemProgressDelayedID = 2;
        public const int actionItemProgressCompltedID = 3;
        public const int actionItemProgressNotStartedID = 4;
        public const string actionItemOnTimeDesc = "On Time";
        public const string actionItemDelayedDesc = "Delayed";
        public const string actionItemCompltedDesc = "Completed";
        public const string actionItemProgressNotStartedDesc = "Not Started";

        public static string giveMecurrentTimeZone()
        {
            return "India Standard Time";
        }
    }
}