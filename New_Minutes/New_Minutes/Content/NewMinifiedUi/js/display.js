$(function () {
    display_meeting_scroll();
    project_details_wrap_scroll();
    cp_ai_update_scroll();
    init();
    /* Create task */
    $(document).on("click", ".create-task-btn.active", function () {
        $(".create-task-input-wrap").fadeOut(300, function () {
            $(".create-task-popup-content").height(60);
            $(".task-created").fadeIn(300);
            $(".task-reset-btn").fadeOut(300);
            $(".create-task-btn").fadeOut(300, function () {
                $(".view-task-btn").fadeIn(300);
                $(".create-task-btn").removeClass("active");
                $(".add-another-task").fadeIn(300);
                $(".add-another-task").addClass("active");
            });
            $(".task-reset-btn").fadeOut(300);
            $(".view-task-btn").addClass("active");

        });
    });
    $(document).on("click", ".add-notesordoc-popup .close-popup,.view-notes-btn.active,.add-another-note.active ", function () {
        /*notes and attachment revers eng code*/
        $(".note-uploaded ").fadeOut(300, function () {
            $(".add-notes-text-wrap").height(170);
            $(".notes-textarea").fadeIn(300);
            $(".view-notes-btn").fadeOut(300, function () {
                $(".upload-notes-btn").fadeIn(300);
                $(".upload-notes-btn").removeClass("active");
                $(".add-another-note").fadeOut(300);

            });

        });
    });

    /*=================================================*/
    /*Task Popup start*/

    /*Open popups from edit popup task*/
    //$(document).on("click", ".edit-task-btn", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /*Open popups from task history*/
    $(document).on("click", ".task-history-btn", function () {
        var el = $(this).data("popup");
        popup_open(el);
    });

    /*attachment-delete popups */
    //$(document).on("click", ".attachment-delete", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});

    /* edit task */
    //$(document).on("click", ".edit-task-btn.active", function () {
    //    $(".edit-task-input-wrap").fadeOut(300, function () {
    //        $(".edit-task-popup-content").css({
    //            "max-height": 60
    //        });
    //        $(".task-edit-1").fadeIn(300);
    //        $(".edit-task-btn").fadeOut(300, function () {
    //            $(".view-task-btn").fadeIn(300);
    //            $(".edit-task-btn").removeClass("active");

    //            //$(".add-another-reminder").fadeIn(300);
    //            //$(".add-another-reminder").addClass("active");
    //        });

    //    });

    //});
    /*=================================================*/
    /* Add Remark */
    //$(document).on("click", ".add-remark-btn.active", function () {
    //    $(".add-remark-input-content").fadeOut(300, function () {
    //        $(".add-remark-popup-content").height(60);
    //        $(".remark-added").fadeIn(300);
    //        $(".add-remark-btn").fadeOut(300, function () {
    //            $(".view-remark-btn").fadeIn(300);
    //            $(".add-remark-btn").removeClass("active");
    //            $(".add-another-remark").fadeIn(300);
    //            $(".add-another-remark").addClass("active");
    //        });
//        $(".view-remark-btn").addClass("active");
//    });
    //});
/* Add Remark */
    $(document).on("click", ".add-remark-btn.active", function () {
        $(".add-remark-input-content").fadeOut(300, function () {
            $(".add-remark-popup-content").height(60);
            $(".remark-added").fadeIn(300);
            $(".add-remark-btn").fadeOut(300, function () {
                $(".view-remark-btn").fadeIn(300);
                $(".add-remark-btn").removeClass("active");
                $(".add-another-remark").fadeIn(300);
                $(".add-another-remark").addClass("active");
            });
         $(".view-remark-btn").addClass("active");
         });
    });

    /* Add remark Reverse*/
    $(document).on("click", ".add-remark-popup .close-popup, .add-another-remark,.view-remark-btn", function () {
    
        $(".remark-added").fadeOut(300, function () {
            $(".add-remark-popup-content").removeAttr('style');
            $(".add-remark-input-content").fadeIn(300);
            $(".view-remark-btn").fadeOut(300, function () {
                $(".add-remark-btn").fadeIn(300);
                $(".add-another-remark").fadeOut(300);
            });
        });

    });

        /* Add sendminutes */
    //$(document).on("click", ".add-sendminutes-btn.active",function(){
    //    $(".add-sendminutes-input-content").fadeOut(300, function(){
    //        $(".add-sendminutes-popup-content").height(60);
    //        $(".sendminutes-added").fadeIn(300);
    //        $(".add-sendminutes-btn").fadeOut(300, function(){
    //            $(".view-sendminutes-btn").fadeIn(300);
    //            $(".add-sendminutes-btn").removeClass("active");
    //            $(".add-another-sendminutes").fadeIn(300);
    //            $(".add-another-sendminutes").addClass("active");
    //        });
            
    //        $(".view-sendminutes-btn").addClass("active");
            
    //    });
    //});
      /* Add sendminutes Reverse*/
    $(document).on("click", ".add-sendminutes-popup .close-popup, .add-sendminutes-remark, .view-log-popup .close-popup",function(){
        $(".sendminutes-added").fadeOut(300, function(){
            $(".add-sendminutes-popup-content").removeAttr('style');
            $(".add-sendminutes-input-content").fadeIn(300);
            $(".view-sendminutes-btn").fadeOut(300, function(){
                $(".add-sendminutes-btn").fadeIn(300);
                $(".add-another-sendminutes").fadeOut(300);
            });
        });
        $(".add-sendminutes-btn").addClass("active");
    });
 

    /* Add Reminder */
   
    /* Add Reminder Reverse*/
    $(document).on("click", ".add-reminder-popup .close-popup, .add-another-reminder,.view-reminder-btn", function () {
        $(".reminder-added").fadeOut(300, function () {
            $(".add-reminder-tab-wrap").fadeIn(300);
            $(".add-reminder-popup-content").removeAttr('style');
            $(".add-reminder-input-wrap").fadeIn(300);
            $(".view-reminder-btn").fadeOut(300, function () {
                $(".add-reminder-btn").fadeIn(300);
                $(".add-another-reminder").fadeOut(300);
            });
        });

    });

      /* Add Escalation */
   
    /* Add escalation Reverse*/
    $(document).on("click", ".add-escalate-popup .close-popup, .add-another-escalation ,.view-escalation-btn", function () {
        $(".escalation-added").fadeOut(300, function () {
            $(".add-escalation-tab-wrap").fadeIn(300);
            $(".add-escalation-popup-content").removeAttr('style');
            $(".add-escalation-input-wrap").fadeIn(300);
            $(".view-escalation-btn").fadeOut(300, function () {
                $(".add-escalation-btn").fadeIn(300);
                $(".add-another-escalation").fadeOut(300);
            });
        });

    });


    //// req to follow js
    
    //$(document).on("click", ".send-request-btn.active", function () {
    //    $(".notiftype-checkbox-wrap").fadeOut(300, function () {
    //        $(".tofollow-popup-content").css({
    //            "max-height": 60
    //        });
    //        $(".request-sent").fadeIn(300);
    //        $(".send-request-btn ").fadeOut(300, function () {
    //           // $(".view-reminder-btn").fadeIn(300);
    //            $(".send-request-btn").removeClass("active");
    //          //  $(".add-another-reminder").fadeIn(300);
    //          //  $(".add-another-reminder").addClass("active");
    //        });
    //      //  $(".view-reminder-btn").addClass("active");
    //    });
    //    $(".tofollow-tab-wrap").fadeOut(300);
    //   // alert('its done');
    //});
    ///* req to follow js Reverse*/
    //$(document).on("click", ".tofollow-popup .close-popup", function () {
    //    $(".request-sent").fadeOut(300, function () {
    //        $(".tofollow-tab-wrap").fadeIn(300);
    //        $(".tofollow-popup-content").removeAttr('style');
    //        $(".notiftype-checkbox-wrap").fadeIn(300);
    //        $(".done-btn").fadeOut(300, function () {
    //            $(".send-request-btn").fadeIn(300);
    //           // $(".add-another-reminder").fadeOut(300);
    //        });
    //    });

    //});
    //$(document).on("click", ".another-task-btn", function () {
        
    //    $(".task-created").fadeOut(300, function () {
    //        $(".add-another-task").removeClass("active");
    //        $(".add-another-task").fadeOut(300);
    //        $(".view-task-btn").fadeOut(300);
    //        $(".view-task-btn").removeClass("active");
    //        $(".create-task-input-wrap").fadeIn(400);
    //           // $(".create-task-popup-content").height(60);
              
    //            $(".task-reset-btn").fadeIn(400);
    //            $(".create-task-btn").fadeIn(400, function () {
                   
    //                $(".create-task-btn").addClass("active");

               
    //            });
    //            $(".task-reset-btn").fadeIn(400);
               

    //        });
    //    });
/* Open popup for view sendminutes for Display Meeting*/
    //  $(document).on("click", ".send-minutes-btn",function(){
	//	var el = $(this).data("popup");
    //    popup_open(el);
	//});

    /* Open popup for Add item for Display Meeting*/
    //$(document).on("click", ".display-action-add", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /* Open popup for create task for Display Meeting*/
    //$(document).on("click", ".display-action-createtask", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /* Open popup for view remark for Display Meeting*/
    $(document).on("click", ".display-action-add-remark, .view-remark-btn", function () {
        var el = $(this).data("popup");
        popup_open(el);
    });
    /* Open popup for view reminder for Display Meeting*/
    //$(document).on("click", ".display-action-add-reminder, .view-reminder-btn", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /*Open popups from Add item for Display Meeting*/
    //$(document).on("click", ".display-add-popup-list", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /*Open popups from Add escalate item for Display Meeting*/
    //$(document).on("click", ".add-escalate1-popup, .view-escalate1-btn", function () {
    //    var el = $(this).data("popup");
    //    popup_open(el);
    //});
    /*Close popup trigger*/
    $(document).on("click", ".k-popup .close-popup", function () {

        popup_close();
    });
});

function display_meeting_scroll() {
    $(".display-meeting-wrap").niceScroll();
}
/* Reminder popup scroll*/
function reminder_popup_scroll() {
    $(".add-reminder-popup .popup-content").perfectScrollbar();
} 
/* Reminder popup scroll*/
function reminder_popup_scroll() {
    $(".add-reminder-popup .popup-content").perfectScrollbar();
}
function project_details_wrap_scroll() {
    $(".project-details-wrap").niceScroll();
}
function cp_ai_update_scroll() {
    $(".cp-ai-update-popup .popup-content ").niceScroll();
}

/*Change add notes and attachment tabs*/
function change_tabs(el, common, target, target_common, callback) {
    if (!el.hasClass("active")) {
        $(target_common).fadeOut(300, function () {
            $(target).fadeIn(300);
            $(target_common).removeClass("enabled");
            $(target).addClass("enabled");
        });
        $(common).removeClass("active");
        el.addClass("active");
        if (callback) {
            callbak();
        }
    }
}

/* Things to be initialized*/
function init() {
    /* Initilization for popup inner elements animation*/

    TweenMax.set($(".k-popup").find(".anim-lr"), {
        opacity: 0,
        x: 50
    });
    TweenMax.set($(".k-popup").find(".anim-bt"), {
        opacity: 0,
        y: 50
    });
    TweenMax.set($(".k-popup").find(".anim-tb"), {
        opacity: 0,
        y: -50
    });
    TweenMax.set(".popup-cover", {
        x: win_w

    });
}


/* Popup Open function */
function popup_inner_open(el) {
    $(el).fadeIn(400);
}
function popup_inner_close(el) {
    $(".k-popup-2").fadeOut(400);
}
function popup_open(el) {

    TweenMax.to(".popup-cover", 0.4, {
        x: 0,
        display: "block",
        force3D: !0,
        ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
        onComplete: function () {
            TweenMax.to(".popup-cover", 0.7, {
                x: -win_w,
                force3D: !0,
                ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                onStart: function () {
                    $(".k-popup").hide();
                    TweenMax.set($(".k-popup").find(".anim-lr"), {
                        opacity: 0,
                        x: 50
                    });
                    TweenMax.set($(".k-popup").find(".anim-bt"), {
                        opacity: 0,
                        y: 50
                    });
                    TweenMax.set($(".k-popup").find(".anim-tb"), {
                        opacity: 0,
                        y: -50
                    });
                    $(el).show();
                    TweenMax.staggerTo($(el).find(".anim-tb"), 0.6, {
                        delay: 0.3,
                        y: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    }, 0.1);
                    TweenMax.staggerTo($(el).find(".anim-lr"), 0.6, {
                        delay: 0.3,
                        x: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    }, 0.1);
                    TweenMax.staggerTo($(el).find(".anim-bt"), 0.6, {
                        delay: 0.3,
                        y: 0,
                        opacity: 1,
                        force3D: !0,
                        ease: new Ease(BezierEasing(0.215, 0.61, 0.355, 1))
                    }, 0.1);
                },
                onComplete: function () {

                    TweenMax.set(".popup-cover", {
                        x: win_w,
                        display: "none"
                    });
                }
            });
        }
    });
}

 function saveEscalations () {
        $(".add-escalation-input-wrap").fadeOut(300, function () {
            $(".add-escalation-popup-content").css({
                "max-height": 60
            });
            $(".escalation-added").fadeIn(300);
            $(".add-escalation-btn").fadeOut(300, function () {
                $(".view-escalation-btn").fadeIn(300);
                $(".add-escalation-btn").removeClass("active");
                $(".add-another-escalation").fadeIn(300);
                $(".add-another-escalation").addClass("active");
                 });
            $(".view-escalation-btn").addClass("active");
            });
        $(".add-escalation-tab-wrap").fadeOut(300);
    };


function saveReminder() {
    $(".add-reminder-input-wrap").fadeOut(300, function () {
        $(".add-reminder-popup-content").css({
            "max-height": 60
        });
        $(".reminder-added").fadeIn(300);
        $(".add-reminder-btn").fadeOut(300, function () {
            $(".view-reminder-btn").fadeIn(300);
            $(".add-reminder-btn").removeClass("active");
            $(".add-another-reminder").fadeIn(300);
            $(".add-another-reminder").addClass("active");
        });
        $(".view-reminder-btn").addClass("active");
    });
    $(".add-reminder-tab-wrap").fadeOut(300);
};
/* Popup Close function */
function popup_close() {
    TweenMax.to(".popup-cover", 0.4, {
        x: 0,
        display: "block",
        force3D: !0,
        ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
        onComplete: function () {
            TweenMax.to(".popup-cover", 0.7, {
                x: -win_w,
                force3D: !0,
                ease: new Ease(BezierEasing(0.55, 0.31, 0.15, 0.93)),
                onStart: function () {
                    $(".k-popup").hide();
                    TweenMax.set($(".k-popup").find(".anim-lr"), {
                        opacity: 0,
                        x: 50
                    });
                    TweenMax.set($(".k-popup").find(".anim-bt"), {
                        opacity: 0,
                        y: 50
                    });
                    TweenMax.set($(".k-popup").find(".anim-tb"), {
                        opacity: 0,
                        y: -50
                    });
                },
                onComplete: function () {

                    TweenMax.set(".popup-cover", {
                        x: win_w,
                        display: "none"
                    });
                }
            });
        }
    });
}