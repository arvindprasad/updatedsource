﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace New_Minutes.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "User name is Required.")]
        //[StringLength(16, ErrorMessage = "Must be between 3 and 16 characters", MinimumLength = 3)]
        public string Username { get; set; }
        [Required(ErrorMessage ="Password is Required.")]
        //[StringLength(18, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        //[RegularExpression(@^ ((?=.*[a - z])(?=.*[A - Z])(?=.*\d)).+$)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }
        public bool RememberMe { get; set; }
    }
    public class ResetPassword {
        [Required(ErrorMessage = "Password is Required.")]
        [StringLength(50, ErrorMessage = "Must be between 4 and 50 Characters.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Confirm Password is Required.")]
        [StringLength(50, ErrorMessage = "Must be between 4 and 50 Characters.", MinimumLength = 4)]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
    }
}