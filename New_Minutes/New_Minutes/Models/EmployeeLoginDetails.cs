﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes.Models
{
    public class EmployeeLoginDetails
    {
        public string login_id { get; set; }
        public int organization_id { get; set; }
        public string employee_ext_code { get; set; }
        public int employee_id { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string email { get; set; }
        public string alt_email { get; set; }
        public int department_id { get; set; }
        public int employee_status_id { get; set; }
        public string mobile_phone_no { get; set; }
        public string land_phone_no { get; set; }
        public string designation { get; set; }
        public int[] app_role_id { get; set; }
        public string[] app_role_short_desc { get; set; }
    }
    public class approleId
    {
        public string app_role_short_desc { get; set; }

    }
    
}