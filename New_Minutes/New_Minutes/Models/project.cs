﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace New_Minutes.Models
{
    public class project
    {
        public int project_id { get; set; }
        public Nullable<int> organization_id { get; set; }
        public string project_name { get; set; }
        public int department_id { get; set; }
        public System.DateTime start_date { get; set; }
        public System.DateTime target_date { get; set; }
        public Nullable<System.DateTime> expected_completion_date { get; set; }
        public Nullable<System.DateTime> actual_completion_date { get; set; }
        public int priority_id { get; set; }
        public int status_id { get; set; }
        public Nullable<bool> sys_project_flag { get; set; }
        public string record_status { get; set; }
        public Nullable<System.DateTime> created_date { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> updated_date { get; set; }
        public string updated_by { get; set; }
    }
    public class mappingdata
    {
        public int project_id { get; set; }
        public string project_name { get; set; }
        public string dept_name { get; set; }
        public string Project_manager { get; set; }
        public string Project_sponsor { get; set; }
        public DateTime Start_Date { get; set; }
        public DateTime target_date { get; set; }
        public string priority_desc { get; set; }

    }
}