﻿using New_Minutes.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace New_Minutes.Controllers
{
    public class TimeSheetAPIController : ApiController
    {
        //InProgress- Save, Pedning--Save&Submitt, Approve-->
        [System.Web.Http.HttpPost]
        public string SaveTimeSheetData(TimeSheetColl _tObj)
        {
            string retVal = string.Empty;
            try
            {

                using (TSEntities TSE = new TSEntities())
                {

                    string Status = _tObj.Status;
                    if (_tObj.TimesheetId <= 0)
                    {
                        //First time Entery
                        //Insert into timesheet with "Status"
                        //Identiy Column will Return the timesheetID
                        timesheet Tsheet = new timesheet();
                        Tsheet.project_id = Convert.ToInt32(_tObj.ProjectID);
                        Tsheet.status = Status;
                        Tsheet.task_owner_id = Convert.ToInt32(_tObj.USerID);
                        Tsheet.weekid = Convert.ToInt32(_tObj.TimeWeekID);
                        Tsheet.year = Convert.ToInt32(_tObj.Year);
                        TSE.timesheets.Add(Tsheet);

                        TSE.SaveChanges();

                        _tObj.TimesheetId = Tsheet.timesheet_id;
                    }
                    else
                    {
                        timesheet timesheet = TSE.timesheets.Where(u => u.timesheet_id == _tObj.TimesheetId).FirstOrDefault<timesheet>(); ;

                        if (timesheet != null)
                        {
                            timesheet.status = _tObj.Status;
                        }
                        TSE.SaveChanges();
                        //Update the Status based on _tObj.TimesheetId
                    }

                    //Delete from timesheet_data where TimesheetId=1
                    //& then Insert

                    var timeSheet = TSE.timesheet_data.Where(u => u.timesheet_id == _tObj.TimesheetId);
                    foreach (var u in timeSheet)
                    {
                        TSE.timesheet_data.Remove(u);
                    }
                    TSE.SaveChanges();

                    foreach (TimeSheetData TS in _tObj.TimeData)
                    {
                        timesheet_data TSD = new timesheet_data();

                        TSD.timesheet_id = Convert.ToInt32(_tObj.TimesheetId);
                        TSD.task_id = Convert.ToInt32(TS.TaskID);
                        TSD.dated = Convert.ToDateTime(TS.Date);
                        TSD.ot = TS.OverTime;
                        TSD.nt = TS.NormalTime;
                        TSD.comment = TS.Comment;
                        TSD.ctrlid = TS.Ctrlid;

                        TSE.timesheet_data.Add(TSD);
                    }
                    TSE.SaveChanges();

                    retVal = "S";
                }
            }
            catch (Exception ex)
            {
                retVal = "F";
            }
            return retVal;
        }


    }
}
