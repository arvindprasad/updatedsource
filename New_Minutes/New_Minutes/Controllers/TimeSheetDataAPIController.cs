﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using New_Minutes.Models;
using System.Text;
using System.Web.Mvc;
using System.Globalization;

namespace New_Minutes.Controllers
{
    public class TimeSheetDataAPIController : ApiController
    {


        [System.Web.Http.HttpPost]
        public object GetTimeSheetData(Newtonsoft.Json.Linq.JObject jsonData)
        {

            int ProjectId = (int)jsonData["ProjectId"];
            int LoogedUserID = (int)jsonData["LoogedUserID"];
            int Year = (int)jsonData["Year"];
            int WeekID = (int)jsonData["WeekID"];

            //Return 
            TimeSheetColl _tObj = new TimeSheetColl();

            _tObj.ProjectID = ProjectId.ToString();
            _tObj.USerID = LoogedUserID.ToString();
            _tObj.Week = WeekID.ToString();
            _tObj.Year = Year.ToString();


            //Get Project Tasks
            TSEntities db = new TSEntities();

            var prjtasks = (from prj in db.tasks
                            where prj.project_id == ProjectId && prj.task_owner_id == LoogedUserID
                            select new { taskid = prj.task_id, task_description = prj.task_description }).OrderBy(x => x.task_description);

            foreach (var itm in prjtasks)
            {
                _tObj.ProjectTask.Add(new ProjectTasks(itm.taskid, itm.task_description));
            }

            //Get Captured TimeSheet
            Int64 timesheetid = 0;
            var prj_timesheet = from time in db.timesheets
                                where time.project_id == ProjectId && time.task_owner_id == LoogedUserID && time.year == Year && time.weekid == WeekID
                                select new { status = time.status, timesheet_id = time.timesheet_id };
            foreach (var itm2 in prj_timesheet)
            {
                if (itm2 != null)
                {
                    _tObj.Status = itm2.status; //Pending//Approved
                    _tObj.TimesheetId = itm2.timesheet_id;
                    timesheetid = itm2.timesheet_id;
                }
            }

            //Get Captured Data
            if (timesheetid > 0)
            {
                var prj_timesheet_data = from data in db.timesheet_data
                                         where data.timesheet_id == timesheetid
                                         select data;
                foreach (var itm2 in prj_timesheet_data)
                {
                    TimeSheetData TSD = new TimeSheetData();
                    TSD.Comment = itm2.comment;
                    TSD.Ctrlid = itm2.ctrlid;
                    TSD.Date = itm2.dated.ToString();
                    TSD.NormalTime = itm2.nt;
                    TSD.OverTime = itm2.ot;
                    TSD.TaskID = itm2.task_id;
                    TSD.TimesheetId = itm2.timesheet_id;
                    _tObj.TimeData.Add(TSD);
                }
            }

            #region "Get All the Other Project timeSheet Data"
            var prj_timesheet_data_other = from ts in db.timesheet_data
                                           join td in db.timesheets on ts.timesheet_id equals td.timesheet_id
                                               into t
                                           from rt in t
                                           where rt.task_owner_id == LoogedUserID && rt.project_id != ProjectId && rt.weekid == WeekID && rt.year == Year
                                           select new { nt = ts.nt, ot = ts.ot, dt = ts.dated };
            List<TimeSheet.OtherProjectData> _oth = new List<TimeSheet.OtherProjectData>(0);
            foreach (var it in prj_timesheet_data_other)
            {
                _oth.Add(new TimeSheet.OtherProjectData(it.dt.ToString(), it.ot == 0 ? null : (decimal?)it.ot, it.nt == 0 ? null : (decimal?)it.nt));
            }

            var results = from mytable in _oth
                          group mytable by new
                          {
                              mytable.Dated
                          } into g
                          select new
                          {
                              dt = g.Key.Dated,
                              Ot = (System.Decimal?)g.Sum(p => p.OverTime),
                              Nt = (System.Decimal?)g.Sum(p => p.NormalTime)
                          };

            foreach (var it in results)
            {
                DateTime dt = Convert.ToDateTime(it.dt);
                string _dtSTR = dt.Day.ToString() + "-" + dt.Month + "-" + dt.Year;

                _tObj.OthePrjData.Add(new TimeSheet.OtherProjectData(_dtSTR, it.Ot == 0 ? 0 : (decimal?)it.Ot, it.Nt == 0 ? 0 : (decimal?)it.Nt));
            }

            #endregion

            JsonResult jsonResult = new JsonResult
            {
                Data = _tObj,
                JsonRequestBehavior = JsonRequestBehavior.AllowGet
            };

            return jsonResult.Data;
        }



    }


}
