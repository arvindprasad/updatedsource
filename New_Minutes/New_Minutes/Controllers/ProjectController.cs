﻿using New_Minutes.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace New_Minutes.Controllers
{
    // [AuthorizationRequired]
    public class ProjectController : ApiController
    {
        public IQueryable getProjectsByEmployeeID(int Employeeid)
        {
            using (var context = new TSEntities())
            {
                var result = from t in context.tasks.Where(w => w.record_status == "A" && w.task_owner_id == Employeeid)
                             join st in context.lkp_status.Where(w => w.record_status == "A" && w.status_short_desc != "TCP" && w.status_category == "TASK") on t.task_status_id equals st.status_id
                             join p in context.projects.Where(w => w.record_status == "A") on t.project_id equals p.project_id
                             //join pr in context.project_resources.Where(w => w.record_status == "A" && w.resource_short_role_desc == "PPM") on p.project_id equals pr.project_id
                             //join emp in context.employees.Where(w => w.record_status == "A") on pr.employee_id equals emp.employee_id
                             select new
                             {
                                 p.project_id,
                                 p.project_name


                             };


                return result.ToList().Distinct().OrderBy(y => y.project_name).AsQueryable();
            }


        }
    }
}

