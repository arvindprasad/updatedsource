﻿using New_Minutes.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace New_Minutes.Controllers
{
    public class Test_ExcelController : Controller
    {
        // GET: Test_Excel
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult ExportData()
        {
            //if (Session["New_Minutes!@#*&^"] != null)
            //{
            var projdata = GetAllProjectsData();
                List<project> SessionData = JsonConvert.DeserializeObject<List<project>>(projdata);
            //}
            GridView gv = new GridView();
            gv.DataSource = SessionData.ToList();
            gv.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=Marklist.xls");
            Response.ContentType = "application/ms-excel";
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            gv.RenderControl(htw);
            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
            return View();
        }
        public static string GetAllProjectsData()
        {
            string result = "";
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:50691");
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            HttpResponseMessage response = client.GetAsync("api/Project").Result;

            if (response.IsSuccessStatusCode)
            {
                var jsonString = response.Content.ReadAsStringAsync();
                return jsonString.Result;

            }
            return result;
        }
        public class project {
            public int project_id { get; set; }
            public Nullable<int> organization_id { get; set; }
            public string project_name { get; set; }
            public int department_id { get; set; }
            public System.DateTime start_date { get; set; }
            public System.DateTime target_date { get; set; }
            public Nullable<System.DateTime> expected_completion_date { get; set; }
            public Nullable<System.DateTime> actual_completion_date { get; set; }
            public int priority_id { get; set; }
            public int status_id { get; set; }
            public Nullable<bool> sys_project_flag { get; set; }
            public string record_status { get; set; }
            public Nullable<System.DateTime> created_date { get; set; }
            public string created_by { get; set; }
            public Nullable<System.DateTime> updated_date { get; set; }
            public string updated_by { get; set; }
        }
    }
}